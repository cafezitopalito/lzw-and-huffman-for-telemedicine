# LZW and Huffman for Telemedicine


## Informações gerais

Disciplina: Algoritimo e Estrutura de Dados II
Professor: Carlo Kleber da Silva Rodrigues
Membros: Beatriz Sofientini Ribeiro,  José Roberto de Oliveira
Universidade Federal do ABC

## Sobre o projeto

Este projeto tem como objetivo analisar e implementar uma aplicação voltada para transmissão de arquivos de exames médicos on-line de emergência ou telemedicina, em redes de comunicação como a Internet utilizando e comparando algoritmos de compressão como LZW e Huffman. Para isso, primeiramente é realizado um estudo teórico comparativo entre os diferentes métodos de compressão de dados apresentados anteriormente. Em seguida, por meio de implementação dos algoritmos de compressão, é feita a análise dos diferentes cenários para o sistema de telemedicina mediante a consideração de que as bases de dados têm tamanhos até 5x maiores do que a memória principal, fazendo a amostragem por ordem de compressão com os diferentes algoritmos apresentados. Os resultados permitem interpretar que o algoritmo LZW possui ótimo desempenho para arquivos de texto, e bom em alguns casos de imagens. Contudo, não comprimindo arquivos de vídeo. Por outro lado, Huffman cumpriu a função de compressão.

- [ ] [Analise do tempo de Compressão e Descompressão](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/compression_time.ipynb)
- [ ] [Arquivos de Controle Experimental](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/Compression/Control)
- [ ] [Tabela com os resultados](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/Results/AED2-Huffman-LZW-Compress-resultados.xlsx)

# Envio SFTP

- [ ] [Simulação de envio de arquivo via SFTP](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/send_sftp.ipynb)
- [ ] [Simulação de recebimento de arquivo via SFTP](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/receive_sftp.ipynb)

# Algoritimo de Huffman
- [ ] [Algoritimo de Huffman](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/python_libs/huffman)
- [ ] [Arquivos provenientes dos testes](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/Compression/Huffman)

# Algoritimo de Lempel-Ziv-Welch
- [ ] [Algoritimo de Lempel-Ziv-Welch](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/python_libs/lzw)
- [ ] [Arquivos provenientes dos testes](https://gitlab.com/cafezitopalito/lzw-and-huffman-for-telemedicine/-/blob/main/Compression/LZW)

